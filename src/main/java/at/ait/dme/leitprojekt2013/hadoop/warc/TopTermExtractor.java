package at.ait.dme.leitprojekt2013.hadoop.warc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * Simple utility to extract top N terms from HTML. <rant>A port from Scala, where the same functionality
 * requires less than half the LOC... #oldskooljava http://plasmasturm.org/log/linesspent/</rant>
 * 
 * @author Rainer Simon <rainer.simon@ait.ac.at>
 */
public class TopTermExtractor {
	
	// English stopwords
	private static Set<String> stopwords_en = new HashSet<String>(Arrays.asList(
			"a", "all", "an", "also", "and", "are", "as", "at", "be", "been", "but", "by", "can", "do", "e", "for", "from", "has",
		    "have", "i", "in", "into", "is", "it", "more", "no", "not", "of", "on", "or", "other", "our", "s", "so", "such", "that", "the", "their",
		    "them", "there", "these", "this", "to", "via", "was", "we", "when", "which", "with", "yes", "you", "your"			
	));

	// German stopwords
	private static Set<String> stopwords_de = new HashSet<String>(Arrays.asList(
			"immer", "er", "wäre", "wieder", "antworten", "wo", "müssen", "muss", "nun", "derstandard", "mal", "ihnen", "geht", "selbst", "melden", "aber", "alles", "als", "am", "an", "at", "auch", "auf", "aus", "bei", "bis", "bitte", "da", "dann", "das", "dass", "daß", "dem", "den", "der",
		    "bin", "kommt", "viel", "des", "denn", "weiter", "die", "durch", "ein", "eine", "einem", "einen", "einer", "eh", "es", "etwas", "fuer", "für", "ganz", "gibt", "hab", "habe", "haben", 
		    "hat", "hätte", "ich", "ihre", "in", "im", "ist", "ja", "jetzt", "kann", "kein", "keine", "können", "man", "mehr", "meiner", "mich", "mir", "mit", "nach", 
		    "nicht", "noch", "nur", "oder", "oft", "permalink", "schon", "sehr", "sich", "sie", "sind", "so", "soll", "um", "und", "über", "von", "war", "was", "wenn", "werden",
		    "wie", "will", "wir", "wird", "wurde", "zu", "zum", "zur", "sein", "vor", "meine", "alle", "damit", "viele", "ihr", "weil", "ab"
	));
	
	// Delimiters used for tokenization
	private static List<String> delimiters = Arrays.asList(
			"\\.", ",", ":", ";", "\\(", "\\)", "<", ">", "!", "\\?", "\"", "“", "'", "/"
	);
	
	// Regex, composed from delimiters
	private static String regex = "\\s*(\\s|" + StringUtils.join(delimiters, "|") + ")\\s*";
	
	/**
	 * Extracts the 10 most frequent terms from the HTML.
	 * @param html the HTML
	 * @return the 10 most frequent terms
	 */
	public static List<TermFrequency> extractFromHTML(String html) {
		return extractFromHTML(html, 10);
	}
	
	/**
	 * Extracts the N most frequent terms from  the HTML.
	 * @param html the HTML
	 * @param n the number of terms to extract
	 * @return the N most frequent terms
	 */
	public static List<TermFrequency> extractFromHTML(String html, int n) {
		// Parse HTML
		Document document = Jsoup.parse(html); 
		
		// Tokenize the text
		List<String> tokenizedText = tokenizeText(document.body().text());
		
		// Compute term frequency
		Map<String, TermFrequency> frequencies = new HashMap<String, TermFrequency>();
		for (String token : tokenizedText) {
			if (!stopwords_en.contains(token) && !stopwords_de.contains(token)) {
				TermFrequency tf = frequencies.get(token);
				if (tf == null)
					tf = new TermFrequency(token, 0);
				
				tf.count += 1;
				frequencies.put(token, tf);
			}
		}
		
		List<TermFrequency> tfValues = new ArrayList<TermFrequency>(frequencies.values());
		Collections.sort(tfValues);
		return new ArrayList<TermFrequency>(tfValues.subList(0, Math.min(n, tfValues.size())));
	}
	
	private static List<String> tokenizeText(String text) {		
		List<String> filtered = new ArrayList<>();
		for (String token :  text.split(regex)) {
			if (isWord(token))
				filtered.add(token.toLowerCase().trim());
		}
		return filtered;
	}
	
	private static boolean isWord(String token) {
		if (token.length() < 2)
			return false;
		
		boolean isNotANumber = false;
		for(char c: token.toCharArray()) {
			if (!Character.isLetterOrDigit(c))
				return false;
			
			if (!Character.isDigit(c))
				isNotANumber = true;				
		}
		return isNotANumber;
	}
	
	public static class TermFrequency implements Comparable<TermFrequency> {
		public String term;
		public int count;
		
		TermFrequency(String term, int count) {
			this.term = term;
			this.count = count;
		}

		@Override
		public int compareTo(TermFrequency other) {
			return other.count - count;
		}
	}
	
}
