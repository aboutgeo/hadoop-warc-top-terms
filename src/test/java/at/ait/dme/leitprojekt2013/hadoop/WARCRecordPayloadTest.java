package at.ait.dme.leitprojekt2013.hadoop;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import at.ait.dme.leitprojekt2013.hadoop.warc.TopTermExtractor;
import at.ait.dme.leitprojekt2013.hadoop.warc.TopTermExtractor.TermFrequency;
import at.ait.dme.leitprojekt2013.hadoop.warc.WarcRecordPayload;

public class WARCRecordPayloadTest {
	
	private static final String FILE_PATH = "src/test/resources/sample.html";

	@Test
	public void testPayloadExtraction() throws IOException {
		String html = StringUtils.join(IOUtils.readLines(new FileInputStream(new File(FILE_PATH))), "\n");
		WarcRecordPayload payload = new WarcRecordPayload(html.replace("\n", "\r\n"));
		
		List<TermFrequency> topTerms = TopTermExtractor.extractFromHTML(payload.getBody(), 20);
		for (TermFrequency tf : topTerms) {
			System.out.println(tf.term + ": " + tf.count);
		}
	}

}
