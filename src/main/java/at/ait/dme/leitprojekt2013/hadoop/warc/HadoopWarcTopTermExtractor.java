package at.ait.dme.leitprojekt2013.hadoop.warc;

import java.io.IOException;
import java.util.List;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.TextOutputFormat;

import edu.cmu.lemurproject.WarcFileInputFormat;
import edu.cmu.lemurproject.WarcRecord;
import edu.cmu.lemurproject.WritableWarcRecord;

import at.ait.dme.leitprojekt2013.hadoop.warc.TopTermExtractor.TermFrequency;

/** 
 * A Hadoop job that reads WARC records and, for each record that contains an HTML page, computes the
 * N most frequent terms in the HTML text.
 * @author Rainer Simon <rainer.simon@ait.ac.at>
 */
public class HadoopWarcTopTermExtractor {
	
	// String constants
	private final static String TYPE_RESPONSE = "response";
	private final static String HEADER_CONTENT_TYPE = "Content-Type";
	private final static String MIMETYPE_HTML = "text/html";
	
	public static class TopTermMapper extends MapReduceBase implements Mapper<LongWritable, WritableWarcRecord, Text, Text> {
		
		@Override
		public void map(LongWritable key, WritableWarcRecord value, OutputCollector<Text, Text> outputCollector, Reporter reporter) throws IOException {
			WarcRecord record = value.getRecord();

			if (record.getHeaderRecordType().equals(TYPE_RESPONSE)) {
				WarcRecordPayload payload = new WarcRecordPayload(new String(record.getContent()));
				
				String contentType = payload.getHeader(HEADER_CONTENT_TYPE);
				if (contentType != null && contentType.startsWith(MIMETYPE_HTML) && payload.getBody() != null) {
					List<TermFrequency> topTerms = TopTermExtractor.extractFromHTML(payload.getBody(), 20);
					
					if (topTerms.size() == 20) {
						// If there are less than 20 terms, it's a useless page (e.g. a 404/3xx notification etc.)
						StringBuffer serialized = new StringBuffer();
						for (TermFrequency tf: topTerms) {
							serialized.append(tf.term + "," + tf.count + "|");
						}
					
						outputCollector.collect(new Text(key.toString()), new Text(serialized.toString()));
					}
				}
			}			
		}
		
	}
	
	public static void main(String[] args) throws Exception {
		JobConf conf = new JobConf(HadoopWarcTopTermExtractor.class);
		conf.setJarByClass(HadoopWarcTopTermExtractor.class);
		conf.setOutputKeyClass(Text.class);
		conf.setOutputValueClass(Text.class);
		conf.setMapperClass(TopTermMapper.class);
		conf.setInputFormat(WarcFileInputFormat.class);
		conf.setOutputFormat(TextOutputFormat.class);
		        
		FileInputFormat.setInputPaths(conf, new Path(args[0]));
		FileOutputFormat.setOutputPath(conf, new Path(args[1]));
		
		JobClient.runJob(conf);
	}
	
}
