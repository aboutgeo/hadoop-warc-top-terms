package at.ait.dme.leitprojekt2013.hadoop.warc;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * A simple wrapper around a WARC record payload string that provides easy access 
 * to headers and body.
 * @author Rainer Simon <rainer.simon@ait.ac.at>
 */
public class WarcRecordPayload {
	
	private Map<String, String> headers = null;
	
	private String body = null;
	
	public WarcRecordPayload(String payload) {
		String normalized = payload.replace("\r", "");
		int bodyStart = normalized.indexOf("\n\n");
		
		if (bodyStart > -1) {
			headers = parseHeaders(normalized.substring(0, bodyStart));
			body = payload.substring(bodyStart + 1).trim();
		} else {
			headers = parseHeaders(payload);
		}
	}
	
	private Map<String, String> parseHeaders(String headerSection) {
		Map<String, String> headers = new HashMap<String, String>();
		
		for (String line : headerSection.split("\n")) {
			String[] header = line.split(":");
			if (header.length == 2)
				headers.put(header[0].trim(), header[1].trim());
		}
		
		return headers;
	}
	
	public Set<String> getHeaders() {
		return headers.keySet();
	}
	
	public String getHeader(String key) {
		return headers.get(key);
	}
	
	public String getBody() {
		return body;
	}

}
