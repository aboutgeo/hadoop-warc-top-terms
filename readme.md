# Hadoop WARC Top-Term Extractor

This is a simple Hadoop project which iterates through collections of WARC Web archive files and extracts the top 20 terms
for each HTML page contained in the WARCs.

## Prerequisites

* The project was tested with Hadoop version 1.1.2. (Note: use the scripts ``bin/start-dfs.sh`` and ``bin/start-mapred-sh`` to start Hadoop.)
* You need [Gradle](http://www.gradle.org/) in order to build the project.

## Quick Start

The project comes with a small sample WARC file and a bash script named ``clean-and-run.sh``. The script:

* builds the project,

* copies the sample file to HDFS,

* runs the MapReduce job, and

* copies the results back to the local file system.

Execute the script from the project folder. If HDFS and Hadoop are set up correctly, the results of the job will be
available in the file ``results/part-00000``. 

## Hacking on this Project

Use ``gradle eclipse`` to create an Eclipse project.

## Troubleshooting

Some notes on problems I encountered (and how I could resolve them):

* If you suspect that Hadoop didn't start properly, you can use the Web UI for HDFS at [http://localhost:50070](http://localhost:50070)
  and/or the MapReduce Admin at [http://localhost:50030](http://localhost:50030) to verify.
  
* To verify that HDFS has been initialized properly, you can use ``hadoop dfs -ls /``

* In my case, HDFS sometimes didn't start properly. In this case, it helped to completely erase the _tmp_ directory that
  backs HDFS (location depends on your setup), and then create a new, blank HDFS using ``hadoop namenode -format``. 
