echo
echo "##################################"
echo "## Rebuilding JAR"
echo "##################################"
gradle jar

echo
echo "##################################"
echo "## Preparing HDFS user directory"
echo "##################################"
rm -rf results
hadoop dfs -rm warcs/sample.warc.gz
hadoop dfs -rmr results

echo
echo "##################################"
echo "## Copying sample WARC to HDFS"
echo "##################################"
hadoop dfs -copyFromLocal src/main/resources/sample.warc.gz warcs/sample.warc.gz

echo
echo "##################################"
echo "## Running MapReduce job"
echo "##################################"
hadoop jar build/libs/hadoop-warc-top-terms.jar at.ait.dme.leitprojekt2013.hadoop.warc.HadoopWarcTopTermExtractor warcs results

echo
echo "##################################"
echo "## Copying results from HDFS"
echo "##################################"
hadoop dfs -copyToLocal results results
echo
